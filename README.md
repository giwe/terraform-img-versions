# STYRIA TERRAFORM - Persist Version

The aim of this Jenkins build is to persist the given artifact version to a
configured GIT repository. This build must not execute concurrently to avoid git merge issues.

# Parameters
Two parameters are required for this build
* module_name: name of the module to update, e.g. 'ms/eureka-service'
* module_version: the version to use, e.g. '21cc8fe6e1093798a571bed7b54acad171d86c38-84'
